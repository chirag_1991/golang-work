package main

import "fmt"

func main() {

	s_data := []int{42, 43, 44, 45, 46, 47, 48, 49, 50}

	for k, v := range s_data {
		fmt.Println(k, ":", v)
	}

	fmt.Printf("%T", s_data)
}
