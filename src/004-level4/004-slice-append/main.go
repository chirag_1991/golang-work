package main

import "fmt"

func main() {

	s_data := []int{42, 43, 44, 45, 46, 47, 48, 49, 50, 51}

	new_data := append(s_data, 52)

	for _, v := range new_data {
		fmt.Println(v)
	}

	extra_data := []int{53, 54, 55}

	new_data = append(new_data, extra_data...)

	for _, v := range new_data {
		fmt.Println(v)
	}
	fmt.Printf("%T", s_data)
}
