package main

import (
	"fmt"
)

func main() {

	x := []string{"James", "Bond", "Shaken, not stirred"}
	y := []string{"Miss", "Moneypenny", "Helllllllooooooooooooo, james"}
	z := [][]string{x, y}

	for _, v := range z {
		fmt.Println(v)
	}
}
