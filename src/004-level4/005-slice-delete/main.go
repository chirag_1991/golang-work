package main

import "fmt"

func main() {

	s_data := []int{42, 43, 44, 45, 46, 47, 48, 49, 50, 51}

	data_after_delete_and_append := append(s_data[:3], s_data[6:]...)
	for k, v := range data_after_delete_and_append {
		fmt.Println(k, ":", v)
	}

	fmt.Printf("%T", data_after_delete_and_append)
}
