package main

import "fmt"

func main() {

	arr := [5]int{}
	arr[0] = 1
	arr[1] = 2
	arr[2] = 10
	arr[4] = 34
	arr[3] = 96

	for v := range arr {
		fmt.Println(v)
	}

	fmt.Printf("%T", arr)
}
