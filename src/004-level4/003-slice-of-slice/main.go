package main

import "fmt"

func main() {

	s_data := []int{42, 43, 44, 45, 46, 47, 48, 49, 50, 51}
	first_data := s_data[0:5]
	second_data := s_data[5:]
	third_data := s_data[2:7]
	fourth_data := s_data[1:6]

	for _, v := range first_data {
		fmt.Print("\t", v)
	}
	fmt.Println("\n")
	for _, v := range second_data {
		fmt.Print("\t", v)
	}
	fmt.Println("\n")
	for _, v := range third_data {
		fmt.Print("\t", v)
	}
	fmt.Println("\n")
	for _, v := range fourth_data {
		fmt.Print("\t", v)
	}
	fmt.Println("\n")
	fmt.Printf("%T", s_data)
}
