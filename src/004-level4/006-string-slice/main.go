package main

import (
	"fmt"
)

func main() {

	x := []string{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"}

	fmt.Println(len(x))
	fmt.Println(cap(x))

	for i := 0; i < len(x); i++ {
		fmt.Println("index at: ", i, " & value is : ", x[i])
	}
}
