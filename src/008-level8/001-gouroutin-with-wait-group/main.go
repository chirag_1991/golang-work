package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	fmt.Println(runtime.NumCPU())
	fmt.Println(runtime.NumGoroutine())
	wg.Add(2)
	go func() {
		fmt.Println("Inside first goroutine")
		wg.Done()
	}()
	fmt.Println(runtime.NumGoroutine())
	go func() {
		fmt.Println("Inside second goroutine")
		wg.Done()
	}()
	fmt.Println(runtime.NumGoroutine())
	wg.Wait()
	fmt.Println("Now exiting")
}
