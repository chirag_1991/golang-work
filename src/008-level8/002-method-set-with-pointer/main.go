package main

import "fmt"

type person struct {
	first string
	last  string
	age   int
}

// If method attached to interface has the reciever pointer than must need to pass it by address
type human interface {
	speak()
}

func (p *person) speak() {
	//fmt.Println("Inside person")
	p.first = "Money"
	p.last = "Penny"
	p.age = 27
}

func saySomething(h human) {
	h.speak()
}

func main() {
	p1 := person{first: "James", last: "Bond", age: 24}
	saySomething(&p1)
	fmt.Println(p1)
}
