package main

import "fmt"

type person struct {
	first string
	last  string
	age   int
}

func changeMe(p *person) {

	p.first = "Money"
	p.last = "Penny"
}

func main() {
	p1 := person{
		first: "James",
		last:  "bond",
		age:   45,
	}

	fmt.Println(p1)

	changeMe(&p1)
	fmt.Println(p1)
}
