package main

import "fmt"

func main() {
	func(i int) {
		fmt.Println("inside anonymus function", i)
	}(35)
}
