package main

import "fmt"

func foo() int {
	return 5
}

func bar() (string, int) {
	return "hello", 5
}
func main() {
	f := foo()
	bs, bi := bar()

	fmt.Println(f)
	fmt.Println(bs)
	fmt.Println(bi)
}
