package main

import "fmt"

func main() {

	x := closureFunc()
	fmt.Println(x())
	fmt.Println(x())
	fmt.Println(x())

	y := closureFunc()
	fmt.Println(y())
	fmt.Println(y())
	fmt.Println(y())
}

func closureFunc() func() int {
	x := 0
	return func() int {
		x++
		return x
	}
}
