package main

import "fmt"

type person struct {
	first string
	last  string
	age   int
}

func (p person) speak() {
	fmt.Println("name:", p.first, p.last)
	fmt.Println("Age:", p.age)
}

func main() {
	p1 := person{first: "James", last: "Bond", age: 57}
	p1.speak()
}
