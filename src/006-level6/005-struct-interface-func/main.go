package main

import "fmt"

type circle struct {
	radius float32
}

type square struct {
	side float32
}

type shape interface {
	area() float32
}

func info(s shape) {
	fmt.Printf("%T : ", s)
	fmt.Println(s.area())
}

func (c circle) area() float32 {
	return ((3.142) * (c.radius) * (c.radius))
}

func (s square) area() float32 {
	return ((s.side) * 2)
}

func main() {
	c := circle{radius: 20.5}
	s := square{side: 20}
	info(c)
	info(s)
}
