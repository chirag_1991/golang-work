package main

import "fmt"

func main() {
	x := []int{10, 11, 12, 13}
	f := foo(x...)
	b := bar(x)

	fmt.Println(f)
	fmt.Println(b)
}

func foo(data ...int) int {
	x := 0
	for _, v := range data {
		x = x + v
	}

	return x
}

func bar(data []int) int {
	x := 0
	for _, v := range data {
		x = x + v
	}

	return x
}
