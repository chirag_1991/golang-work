package main

import (
	"fmt"
)

func on_exiting() {
	fmt.Println("Now exiting")
}

func inside_main() {
	fmt.Println("Called after defer defination")
}
func main() {
	defer on_exiting()
	inside_main()
}
