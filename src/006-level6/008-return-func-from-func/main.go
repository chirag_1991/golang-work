package main

import "fmt"

func main() {

	g := funcOfFunc()
	h := g(1)
	fmt.Println(h)
}

func funcOfFunc() func(i int) int {

	return func(i int) int {
		return i
	}
}
