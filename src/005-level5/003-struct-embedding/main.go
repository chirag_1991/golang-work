package main

import (
	"fmt"
	"runtime"
)

type vehicle struct {
	doors int
	color string
}

type truck struct {
	vehicle
	fourWheel bool
}

type sedan struct {
	vehicle
	luxury bool
}

func (s sedan) funcsedan() string {
	s1 := "Hi, this is sedan"
	return s1
}

func (t truck) functruck() string {
	t1 := "Hi, this is truck"
	return t1
}
func main() {
	fmt.Printf("%d", runtime.NumCPU)
	t := truck{
		vehicle:   vehicle{doors: 2, color: "black"},
		fourWheel: true,
	}
	s := sedan{
		vehicle: vehicle{doors: 4, color: "black"},
		luxury:  true,
	}

	fmt.Println(t.doors)
	fmt.Println(s.doors)
}
