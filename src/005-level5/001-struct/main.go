package main

import "fmt"

type Person struct {
	firstname string
	lastname  string
	icecream  []string
}

func main() {

	p1 := Person{firstname: "Chirag", lastname: "Rajput", icecream: []string{"Choclate", "Choco-chip"}}
	p2 := Person{firstname: "James", lastname: "Bond", icecream: []string{"Vanilla", "Strawberry"}}

	fmt.Println(p1.firstname)
	fmt.Println(p1.lastname)
	for _, v := range p1.icecream {
		fmt.Println(v)
	}
	fmt.Println(p2.firstname)
	fmt.Println(p2.lastname)
	for _, v := range p2.icecream {
		fmt.Println(v)
	}
}
