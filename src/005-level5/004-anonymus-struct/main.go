package main

import (
	"fmt"
)

func main() {

	s := struct {
		v1 map[string]int
		v2 []int
	}{
		v1: map[string]int{"key1": 1, "key2": 2},
		v2: []int{1, 2, 3, 4},
	}

	fmt.Println(s)

	for k, v := range s.v1 {
		fmt.Println(k, v)
	}
	for k, v := range s.v2 {
		fmt.Println(k, v)
	}
}
