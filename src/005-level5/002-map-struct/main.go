package main

import "fmt"

type Person struct {
	firstname string
	lastname  string
	icecream  []string
}

func main() {
	p1 := Person{firstname: "Chirag", lastname: "Rajput", icecream: []string{"Choclate", "Choco-chip"}}
	p2 := Person{firstname: "James", lastname: "Bond", icecream: []string{"Vanilla", "Strawberry"}}
	xp := map[string]Person{p1.lastname: p1, p2.lastname: p2}

	for _, v := range xp {
		fmt.Println(v.firstname)
		fmt.Println(v.lastname)

		for _, xv := range v.icecream {
			fmt.Println(xv)
		}
	}
}
